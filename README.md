# Twitter Crawler Project

## Introduction
This project is developed for crawling Twitter website and extracting accounts' tweets from their profile. Due to the dynamic behaviour of Twitter's website and need for Javascript to run, utilizing traditional crawling methods like making a simple request and parsing the result is not effective anymore. Hence, Selenium is used as a headless browser for crawling Twitter.

### Components
* **Crawlers**:
    * **Selenium standalone**: Selenium's explicit waits makes us able to wait for an element to load. This feature is useful in dynamic webpages that AJAX requests are made to server after page is loaded. **BeatifulSoup** is used for parsing the fetched page from Selenium.
    * **Scrapy with Selenium**: In order to increase the efficiency and performance, Scrapy is used alongside Selenium. "scrapy-selenium" module is used for integrating them together. 
* **Apache Kafka**: This streaming platform is used for sending crawled data files from a producer to a consumer.
* **Selenium**: Selenium provides powerful features for web browsing automation. It will give us all the required features for crawling dynamic web pages like Twitter.

### Installing and Running
All the required modules for running the projects are located in ```requirements.txt``` file. For installing the modules run the following command:
```
pip install -r requirements.txt
```
Apache Kafka, Zookeeper, and Selenium are available on docker. So, in order to run them simply run the following command at the root of the project:
```
docker-compose up -d
```
This command will build Apache Kafka, Zookeeper, and Selenium images and run them.



