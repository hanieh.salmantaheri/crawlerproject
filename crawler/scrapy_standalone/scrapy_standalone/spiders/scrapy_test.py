import scrapy
import json
from scrapy_selenium import SeleniumRequest
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

class TwitterSpider(scrapy.Spider):
    name = 'tSpider'
    def start_requests(self):
        yield SeleniumRequest(
            url="https://twitter.com/henryconyekuru",
            wait_time=30,
            #wait_until= lambda x: x.find_element_by_css_selector("div[data-testid='tweet']"),
            wait_until=EC.presence_of_all_elements_located((By.XPATH,'//*[@id="react-root"]/div/div/div[2]/main/div/div/div/div/div/div[2]/div/div/div[2]/section/div/div/div[1]/div/div/article/div/div/div/div[2]')),
            script='window.scrollTo(0, document.body.scrollHeight);',
            screenshot=True,
            callback=self.parse,
            dont_filter=True
        )
    def parse(self, response):
        user_name = response.selector.xpath('//*[@id="react-root"]/div/div/div[2]/main/div/div/div/div/div/div[1]/div[1]/div/div/div/div/div[2]/div/h2/div/div/div/span[1]/span/span/text()').get()
        user_id = response.selector.xpath('//*[@id="react-root"]/div/div/div[2]/main/div/div/div/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/div[2]/div/span/text()').get()
        user_description = response.selector.xpath('//*[@id="react-root"]/div/div/div[2]/main/div/div/div/div/div/div[2]/div/div/div[1]/div[2]/div[3]/div/div//text()').get()
        userProfileHeader_Items = response.selector.xpath('//*[@id="react-root"]/div/div/div[2]/main/div/div/div/div/div/div[2]/div/div/div[1]/div[2]/div[4]/div//text()').get()
        tweet_numbers = response.selector.xpath('//*[@id="react-root"]/div/div/div[2]/main/div/div/div/div/div/div[1]/div[1]/div/div/div/div/div[2]/div/div/text()').get()
        yield {'user_name': user_name,
               'user_id' : user_id,
               'user_description' : user_description,
               'more_info': userProfileHeader_Items,
               'tweet_count':tweet_numbers}


